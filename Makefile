PACKAGE=arch-pacman
PACMANVER=6.0.1
DEBREL=1

SRCDIR=pacman
SRCTAR=${SRCDIR}.tgz

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)
GITVERSION:=$(shell cat .git/refs/heads/master)

DEB=${PACKAGE}_${PACMANVER}-${DEBREL}_amd64.deb
DEB2=${PACKAGE}-dev_${PACMANVER}-${DEBREL}_amd64.deb \
     ${PACKAGE}-dbgsym_${PACMANVER}-${DEBREL}_amd64.deb

DEBS= ${DEB} ${DEB2}

all: ${DEBS}
	echo ${DEBS}

${SRCDIR}: ${SRCTAR}
	rm -rf ${SRCDIR}.tmp
	mkdir ${SRCDIR}.tmp
	tar xf ${SRCTAR} -C ${SRCDIR}.tmp --strip-components 1
	cp -a debian ${SRCDIR}.tmp/debian
	mv ${SRCDIR}.tmp ${SRCDIR}

.PHONY: builddir
builddir:
	rm -rf ${SRCDIR}
	$(MAKE) ${SRCDIR}

${DEB}: builddir
	echo "git clone https://gitlab.archlinux.org/pacman/pacman.git\\ngit checkout ${GITVERSION}" >  ${SRCDIR}/debian/SOURCE
	cd ${SRCDIR}; DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -rfakeroot -b -us -uc
	lintian ${DEBS}

.PHONY: deb
deb ${DEB2}: ${DEB}

.PHONY: download
download: ${SRCTAR}
${SRCTAR}:
	rm -rf ${SRCDIR} ${SRCTAR} pacman-git
	git clone --depth=1 --branch=v${PACMANVER} https://gitlab.archlinux.org/pacman/pacman.git pacman-git
	(cd pacman-git && git archive --prefix=${SRCDIR}/ --format=tar v${PACMANVER}) | gzip > ${SRCTAR}.tmp
	mv ${SRCTAR}.tmp ${SRCTAR}
	rm -rf pacman-git

distclean: clean
	rm -rf ${SRCDIR} ${SRCTAR} pacman-git

.PHONY: clean
clean:
	rm -rf ${SRCDIR} ${SRCDIR}.tmp *_${ARCH}.deb *.changes *.dsc *.buildinfo
	find . -name '*~' -exec rm {} ';'

.PHONY: dinstall
dinstall: ${DEBS}
	dpkg -i ${DEBS}
